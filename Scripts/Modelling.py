#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 16:11:26 2021

@author: naina.alex1
"""

#%%
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import seaborn as sns    
from geopy.geocoders import Nominatim
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import xgboost as xgb
from sklearn import datasets
from sklearn.model_selection import train_test_split
import joblib
from sklearn.metrics import precision_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn import metrics
import seaborn as sb
from geopy import distance
from sklearn.metrics import accuracy_score
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe

#%%
file = pd.read_csv(r'/Users/naina.alex1/Downloads/Lead Scoring model/Model_data_new.csv')

#%%
K_clusters = range(1,10)
kmeans = [KMeans(n_clusters=i) for i in K_clusters]
Y_axis = file[['location_latitude']]
X_axis = file[['location_longitude']]
score = [kmeans[i].fit(Y_axis).score(Y_axis) for i in range(len(kmeans))]
# Visualize
plt.plot(K_clusters, score)
plt.xlabel('Number of Clusters')
plt.ylabel('Score')
plt.title('Elbow Curve')
plt.show()

kmeans = KMeans(n_clusters = 4, init ='k-means++')
kmeans.fit(file[file.columns[-2:]])

file["LOCATION_SET"] = np.nan

for i in range(len(file)):
    
    if file['location_latitude'][i] != 'NA':    
        file["LOCATION_SET"][i] = kmeans.predict(np.array(file[["location_latitude","location_longitude"]].iloc[i]).reshape(1,-1))
    else:
        file["LOCATION_SET"][i] = 0
        
file['REASON_FOR_PURCHASE_SET']=np.nan
file['reason_for_purchase']=file['reason_for_purchase'].replace(np.nan,'NA')

for i in range(len(file)):
    if file['reason_for_purchase'][i] in ('Moving in to a better location'):
        file['REASON_FOR_PURCHASE_SET'][i] = 'Set1'
    elif file['reason_for_purchase'][i] in ('Moving in to a bigger home','Moving for a better lifestyle'):
        file['REASON_FOR_PURCHASE_SET'][i] = 'Set2'
    elif file['reason_for_purchase'][i] in ('Investment - will rent it out'):
        file['REASON_FOR_PURCHASE_SET'][i] = 'Set3'
    else:
        file['REASON_FOR_PURCHASE_SET'][i] = 'Set4'
        
#%%
def cut_off(model,X,y):
    predict_proba = pd.DataFrame(model.predict_proba(X))
    pred_log = model.predict(X)
    pred_log = pd.DataFrame(pred_log)
    Y1_test1 = y.reset_index()
    predictions = pd.concat([Y1_test1,pred_log,predict_proba],axis = 1)
    predictions.columns = ['index', 'actual', 'predicted', 'Purchased_0', 'Purchased_1']
    
    auc_score = metrics.roc_auc_score( predictions.actual, predictions.Purchased_1 )
    print(round( float( auc_score ), 2 ))
    
    fpr, tpr, threshold = metrics.roc_curve(y,predictions.Purchased_1,drop_intermediate=False)
    roc_auc = metrics.auc(fpr, tpr)

    plt.title('Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label='ROC curve (area = %0.2f)' % auc_score)
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    
    return predictions

def new_lab(predictions,cutoff):
    predictions['new_labels'] = predictions['Purchased_1'].map( lambda x: 1 if x >= cutoff else 0 )
    print(classification_report(predictions.actual,predictions.new_labels)) 
    cm1 = metrics.confusion_matrix( predictions.actual,
                              predictions.new_labels, [0,1] )
    sb.heatmap(cm1, annot=True,  fmt='.2f', xticklabels = [0,1] , yticklabels = [0,1])
    plt.ylabel('True label',fontsize=12)
    plt.xlabel('Predicted label',fontsize=12)
    return predictions

def decile(X,y,model):
    df_deciles=pd.DataFrame(y)
    df_deciles['Probability']=model.predict_proba(X)[:,1]
    df_deciles['decile']=pd.qcut(df_deciles['Probability'],10,labels=['10', '9', '8', '7', '6', '5', '4', '3', '2','1'])
    df_deciles.columns = ['Buyers','Probability','Decile']
    df_deciles['Non-Buyers'] = 1-df_deciles['Buyers']

    df1 = pd.pivot_table(data=df_deciles,index=['Decile'],values=['Buyers','Non-Buyers','Probability'],
                     aggfunc={'Buyers':[np.sum],
                              'Non-Buyers':[np.sum],
                              'Probability' : [np.min,np.max]})
    
    df1.reset_index()
    df1.columns = ['Buyers_Count','Non-Buyers_Count','max_score','min_score']
    df1['Total_Cust'] = df1['Buyers_Count']+df1['Non-Buyers_Count']
    
    df2 = df1.sort_values(by='min_score',ascending=False)
    df2['Buyer_Rate'] = (df2['Buyers_Count'] / df2['Total_Cust']).apply('{0:.2%}'.format)
    df2['Non-Buyer_Rate'] = (df2['Non-Buyers_Count'] / df2['Total_Cust']).apply('{0:.2%}'.format)
    
    default_sum = df2['Buyers_Count'].sum()
    non_default_sum = df2['Non-Buyers_Count'].sum()
    df2['Buyers %'] = (df2['Buyers_Count']/default_sum).cumsum().apply('{0:.2%}'.format)
    df2['Non_Buyers %'] = (df2['Non-Buyers_Count']/non_default_sum).cumsum().apply('{0:.2%}'.format)
    
    df2['ks_stats'] = np.round(((df2['Buyers_Count'] / df2['Buyers_Count'].sum()).cumsum() -(df2['Non-Buyers_Count'] / df2['Non-Buyers_Count'].sum()).cumsum()), 4) * 100
    flag = lambda x: '*****' if x == df2['ks_stats'].max() else ''
    df2['max_ks'] = df2['ks_stats'].apply(flag)
    return df2

#%%
Required_file = file[["budget","unit_type","source_bucketing","Project","selfuse_inv",
                      "home_to_project_dist", "dep_var"]]

#%%
X = Required_file[["budget","unit_type","source_bucketing","Project","selfuse_inv",
                      "home_to_project_dist"]]
y = Required_file["dep_var"]

#%%
x_1 = pd.get_dummies(X, columns = ["budget","unit_type","source_bucketing","Project","selfuse_inv"])
x1_train, x1_test, y1_train, y1_test = train_test_split(x_1, y, test_size=0.2)

#%%
space={'max_depth': hp.quniform("max_depth", 3, 15, 1),
        'gamma': hp.uniform ('gamma', 1,9),
        'reg_alpha' : hp.quniform('reg_alpha', 40,180,1),
        'reg_lambda' : hp.uniform('reg_lambda', 0,1),
        'colsample_bytree' : hp.uniform('colsample_bytree', 0.5, 1),
        'min_child_weight' : hp.quniform('min_child_weight', 0, 10, 1),
        'n_estimators': 180,#hp.quniform('n_estimators', 30, 150, 30),
        'seed': 0,
       'scale_pos_weight' : hp.quniform('scale_pos_weight', 0, 10, 2),
       #'learning_rate' : hp.quniform('learning_rate', 0.05, 0.3, 0.05)
      }
def objective(space):
    clf=xgb.XGBClassifier(
                    max_depth = int(space['max_depth']), gamma = space['gamma'],
                    reg_alpha = int(space['reg_alpha']),min_child_weight=int(space['min_child_weight']),
                    colsample_bytree=int(space['colsample_bytree']),n_estimators=space['n_estimators'],
                    scale_pos_weight=int(space['scale_pos_weight']))
    
    evaluation = [( x1_train, y1_train), ( x1_test, y1_test)]
    
    clf.fit(x1_train, y1_train,
            eval_set=evaluation, eval_metric="auc",
            early_stopping_rounds=10,verbose=False)
    

    pred = clf.predict(x1_test)
    accuracy = accuracy_score(y1_test, pred>0.5)
    print ("SCORE:", accuracy)
    return {'loss': -accuracy, 'status': STATUS_OK }

trials = Trials()

best_hyperparams = fmin(fn = objective,
                        space = space,
                        algo = tpe.suggest,
                        max_evals = 100,
                        trials = trials)

print("The best hyperparameters are : ","\n")
print(best_hyperparams)
#%%
model_baysean = XGBClassifier(colsample_bytree = 0.7073370496969509,
                              gamma = 4.294669379388092, max_depth = 10,
                              min_child_weight = 6, reg_alpha = 40, 
                              reg_lambda = 0.07869468008604823, scale_pos_weight = 7).fit(x1_train,y1_train)


#%%
pred_val_bay = model_baysean.predict(x1_test)
print(classification_report(y1_test,pred_val_bay))

cm1 = metrics.confusion_matrix(y1_test, pred_val_bay, [0,1] )
sb.heatmap(cm1, annot=True,  fmt='.2f', xticklabels = [0,1] , yticklabels = [0,1])
#%%
df_bay_val=decile(x1_test,y1_test,model_baysean)

pred_bay_val=cut_off(model_baysean,x1_test,y1_test)
#%%
#decile(x1_test, y1_test, model_baysean).to_csv(r'/Users/naina.alex1/Downloads/Lead Scoring model/KS.csv')

#%%
import shap
explainer = shap.TreeExplainer(model_baysean)
#%%
# Calculate Shap values
choosen_instance = x1_test.loc[10380] #.round(2)
shap_values = explainer.shap_values(choosen_instance, y1_test.loc[10380], output_margin=True)
shap.initjs()
shap.force_plot(explainer.expected_value[1], shap_values[1], choosen_instance, matplotlib=True)
 
#%%
shap_values = shap.TreeExplainer(model_baysean).shap_values(x1_train)
shap.summary_plot(shap_values, x1_train)

#%%
import sweetviz as sv
my_report_LS = sv.analyze(Required_file, target_feat = 'dep_var')
#my_report_LS.show_html()
















